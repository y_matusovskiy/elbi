﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Communication.Enums
{
    public enum ProxyCommand
    {
        InitiatorStarted = 100,
        InitiatorStopped = 10,
        InitiatorExited = 0
    }
}
