﻿using System;
using System.Net;

namespace Communication
{
    public interface IMultithreadedHttpClient
    {
        event Action<HttpWebResponse> ProcessResponse;

        void Dispose();
        void Enqueue(HttpWebRequest webRequest);
        void Start();
        void Stop();
    }
}