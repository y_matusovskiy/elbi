﻿using System;
using System.Net;

namespace Communication
{
    public interface IMultithreadedHttpServer
    {
        event Action<HttpListenerContext> ProcessRequest;

        void Dispose();
        void Start(int port);
        void Stop();
    }
}