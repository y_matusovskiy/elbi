﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Communication
{
    public class MultithreadedHttpClient : IDisposable, IMultithreadedHttpClient
    {
        private readonly Thread[] workers;
        private Queue<HttpWebRequest> queue;

        private volatile bool isRunning;

        public event Action<HttpWebResponse> ProcessResponse;

        public MultithreadedHttpClient(int numOfThreads = 1)
        {
            workers = new Thread[numOfThreads];
            queue = new Queue<HttpWebRequest>();
        }

        public void Dispose()
        {
            Stop();
        }

        public void Start()
        {
            isRunning = true;

            for (int i = 0; i < workers.Length; i++)
            {
                workers[i] = new Thread(Worker);
                workers[i].Start();
            }
        }

        public void Enqueue(HttpWebRequest webRequest)
        {
            lock(queue)
            {
                queue.Enqueue(webRequest);
            }
        }

        public void Stop()
        {
            isRunning = false;

            foreach (Thread worker in workers)
            {
                worker.Join();
            }
        }

        private void Worker()
        {
            while (isRunning || queue.Count > 0)
            {
                HttpWebRequest request;
                lock (queue)
                {
                    if (queue.Count > 0)
                    {
                        request = queue.Dequeue();
                    }
                    else
                    {
                        continue;
                    }
                }

                HttpWebResponse response = (HttpWebResponse)request.GetResponse();

                ProcessResponse(response);
            }
        }
    }
}
