﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Communication
{
    public class MultithreadedHttpServer : IDisposable, IMultithreadedHttpServer
    {
        private readonly HttpListener listener;
        private readonly Thread listenerThread;
        private readonly Thread[] workers;
        private readonly ManualResetEvent stop, ready;
        private Queue<HttpListenerContext> queue;

        public event Action<HttpListenerContext> ProcessRequest;

        public MultithreadedHttpServer(int numOfThreads = 1)
        {
            workers = new Thread[numOfThreads];
            queue = new Queue<HttpListenerContext>();
            stop = new ManualResetEvent(false);
            ready = new ManualResetEvent(false);
            listener = new HttpListener();
            listenerThread = new Thread(HandleRequests);
        }

        public void Start(int port)
        {
            listener.Prefixes.Add(String.Format(@"http://localhost:{0}/", port));
            listener.Start();
            listenerThread.Start();

            for (int i = 0; i < workers.Length; i++)
            {
                workers[i] = new Thread(Worker);
                workers[i].Start();
            }
        }

        public void Dispose()
        {
            Stop();
        }

        public void Stop()
        {
            stop.Set();
            listenerThread.Join();
            foreach (Thread worker in workers)
            {
                worker.Join();
            }
            listener.Stop();
        }

        private void HandleRequests()
        {
            while (listener.IsListening)
            {
                var context = listener.BeginGetContext(ContextReady, null);

                var wait = new[] { stop, context.AsyncWaitHandle };
                if (0 == WaitHandle.WaitAny(wait))
                    return;
            }
        }

        private void ContextReady(IAsyncResult ar)
        {
            try
            {
                lock (queue)
                {
                    queue.Enqueue(listener.EndGetContext(ar));
                    ready.Set();
                }
            }
            catch { return; }
        }

        private void Worker()
        {
            WaitHandle[] wait = new[] { ready, stop };
            while (0 == WaitHandle.WaitAny(wait))
            {
                HttpListenerContext context;
                lock (queue)
                {
                    if (queue.Count > 0)
                        context = queue.Dequeue();
                    else
                    {
                        ready.Reset();
                        continue;
                    }
                }

                try
                {
                    ProcessRequest(context);
                }
                catch (Exception e)
                {
                    Console.WriteLine(e);
                }
            }
        }
    }
}
