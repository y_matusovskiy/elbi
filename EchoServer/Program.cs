﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using Communication;
using NDesk.Options;
using System.Threading;
using IO;

namespace EchoServer
{
    class Program
    {
        // to mock command line arguments up in debug mode:
        // go to Project Properties -> Debug -> Command Line Arguments
        // Example:
        // -p=http://localhost:17001 -t=3

        private const int _port = 17003;

        private static string proxyAddress;
        private static int numOfThreads;

        private static List<int> echoedNumbers;

        private static MultithreadedHttpServer httpServer;
        private static FileWriter echoFileWriter;

        static void Main(string[] args)
        {
            Console.WriteLine("EchoServer\n");
            Console.WriteLine($"Command line arguments: {string.Join(" ", args)}\n");

            try
            {
                InitializeArguments(args);
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                Console.WriteLine("Terminating...");
                Console.ReadKey();
                return;
            }

            InitializeGlobals();

            Console.WriteLine("Feel free to use any of the given commands: start, exit");

            while (true)
            {
                var command = Console.ReadLine();
                switch (command)
                {
                    case "start":
                        httpServer.Start(_port);

                        Console.WriteLine($"Started on port {_port}");
                        break;
                    case "exit":
                        httpServer.Stop();
                        WriteOutput();

                        Console.WriteLine("Terminating...");
                        return;
                    default:
                        Console.WriteLine("Unknown command: " + command);
                        break;
                }
            }
        }

        private static void InitializeGlobals()
        {
            echoedNumbers = new List<int>();

            httpServer = new MultithreadedHttpServer(numOfThreads);
            httpServer.ProcessRequest += HttpServer_ProcessRequest;

            echoFileWriter = new FileWriter("echo.txt");
        }

        private static void HttpServer_ProcessRequest(HttpListenerContext context)
        {
            string requestBody;

            using (var streamReader = new StreamReader(context.Request.InputStream))
            {
                requestBody = streamReader.ReadToEnd();
                streamReader.Close();
            }

            lock (echoedNumbers)
            {
                var number = int.Parse(requestBody);
                echoedNumbers.Add(number);
            }

            using (var streamWriter = new StreamWriter(context.Response.OutputStream))
            {
                streamWriter.Write(requestBody);
                streamWriter.Close();
            }

            context.Response.StatusCode = 200;
            context.Response.Close();
        }

        /// <exception cref="OptionException">Thrown if an argument can not be parsed</exception>
        /// <exception cref="ArgumentException">Thrown if any of the arguments is unassigned or number of sender/receiver threads is more than 3</exception>
        private static void InitializeArguments(string[] args)
        {
            var options = new OptionSet() {
                { "p|proxy=", "Proxy Server's IP/Port", p => proxyAddress = p},
                { "t=", "Threads count", (int t) => numOfThreads = t}
            };

            options.Parse(args);

            if (string.IsNullOrEmpty(proxyAddress)
                || numOfThreads == 0 || numOfThreads > 3)
            {
                throw new ArgumentException("Incorrect command line arguments");
            }
        }

        private static void WriteOutput()
        {
            echoedNumbers.Sort();

            echoFileWriter.Write(echoedNumbers);
        }
    }
}
