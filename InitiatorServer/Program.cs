﻿using Communication;
using Communication.Enums;
using IO;
using NDesk.Options;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace InitiatorServer
{
    class Program
    {
        // to mock command line arguments up in debug mode:
        // go to Project Properties -> Debug -> Command Line Arguments
        // Example:
        // -p=http://localhost:17001 -t=3

        private static FileWriter sendFileWriter;
        private static FileWriter receiveFileWriter;

        private static List<int> sendNumbers;
        private static List<int> receivedNumbers;

        private static string proxyAddress;
        private static int numOfThreads;

        private static int emittedNumber;
        private static Thread numberEmitterThread;
        private static volatile bool isEmitting;

        private static MultithreadedHttpClient httpClient;

        static void Main(string[] args)
        {
            Console.WriteLine("InitiatorServer\n");
            Console.WriteLine($"Command line arguments: {string.Join(" ", args)}\n");

            try
            {
                InitializeArguments(args);
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                Console.WriteLine("Terminating...");
                Console.ReadKey();
                return;
            }

            InitializeGlobals();

            Console.WriteLine("Feel free to use any of the given commands: start, stop, exit");

            while (true)
            {
                var command = Console.ReadLine();
                switch (command)
                {
                    case "start":
                        NotifyProxy(ProxyCommand.InitiatorStarted);

                        httpClient.Start();

                        numberEmitterThread = new Thread(EmitNumbers);
                        isEmitting = true;
                        numberEmitterThread.Start();

                        Console.WriteLine($"Started");
                        break;
                    case "stop":
                        Console.WriteLine("Stopping... Can take some time to finish pending requests...");

                        isEmitting = false;
                        numberEmitterThread.Join();

                        httpClient.Stop();

                        WriteOutputToFiles();

                        NotifyProxy(ProxyCommand.InitiatorStopped);

                        Console.WriteLine("Stopped");
                        break;
                    case "exit":
                        Console.WriteLine("Stopping... Can take some time to finish pending requests...");

                        isEmitting = false;
                        numberEmitterThread.Join();

                        httpClient.Stop();

                        WriteOutputToFiles();

                        NotifyProxy(ProxyCommand.InitiatorExited);

                        Console.WriteLine("Terminating...");
                        return;
                    default:
                        Console.WriteLine("Unknown command: " + command);
                        break;
                }
            }
        }

        private static void InitializeGlobals()
        {
            sendFileWriter = new FileWriter("initiator_send.txt");
            receiveFileWriter = new FileWriter("initiator_receive.txt");

            sendNumbers = new List<int>();
            receivedNumbers = new List<int>();

            httpClient = new MultithreadedHttpClient(numOfThreads);
            httpClient.ProcessResponse += HttpClient_ProcessResponse;

            emittedNumber = 0;
        }

        private static void HttpClient_ProcessResponse(HttpWebResponse response)
        {
            if (response.StatusCode == HttpStatusCode.OK)
            {
                string responseBody;
                using (var streamReader = new StreamReader(response.GetResponseStream()))
                {
                    responseBody = streamReader.ReadToEnd();
                }

                lock (receivedNumbers)
                {
                    var number = int.Parse(responseBody);
                    receivedNumbers.Add(number);
                }
            }

            response.Close();
        }

        private static void EmitNumbers()
        {
            while (isEmitting)
            {
                try
                {
                    HttpWebRequest request = (HttpWebRequest)WebRequest.Create(proxyAddress);

                    request.Method = "POST";
                    request.Host = "localhost:17003";

                    using (var streamWriter = new StreamWriter(request.GetRequestStream()))
                    {
                        streamWriter.Write(emittedNumber);
                        streamWriter.Close();
                    }

                    sendNumbers.Add(emittedNumber);
                    httpClient.Enqueue(request);

                    emittedNumber++;
                }
                catch(Exception e)
                {
                    Console.WriteLine("Not able to connect to proxy");
                    Console.WriteLine(e.Message);
                }
            }
        }

        private static void InitializeArguments(string[] args)
        {
            var options = new OptionSet() {
                { "p|proxy=", "Proxy Server's IP/Port", p => proxyAddress = p},
                { "t=", "Threads count", (int t) => numOfThreads = t}
            };

            options.Parse(args);

            if (string.IsNullOrEmpty(proxyAddress)
                || numOfThreads == 0 || numOfThreads > 3)
            {
                throw new ArgumentException("Incorrect command line arguments");
            }
        }
        
        private static void WriteOutputToFiles()
        {
            receivedNumbers.Sort();

            sendFileWriter.Write(sendNumbers);
            receiveFileWriter.Write(receivedNumbers);
        }

        private static void NotifyProxy(ProxyCommand proxyCommand)
        {
            HttpWebRequest request = (HttpWebRequest)WebRequest.Create(proxyAddress);

            request.Method = "POST";

            try
            {
                using (var streamWriter = new StreamWriter(request.GetRequestStream()))
                {
                    streamWriter.Write((int)proxyCommand);
                    streamWriter.Close();
                }

                HttpWebResponse response = (HttpWebResponse)request.GetResponse();

                if (response.StatusCode == HttpStatusCode.OK)
                {
                    string responseBody;
                    using (var streamReader = new StreamReader(response.GetResponseStream()))
                    {
                        responseBody = streamReader.ReadToEnd();
                    }

                    Console.WriteLine("Proxy is notified");
                }
            }
            catch(Exception e)
            {
                Console.WriteLine("Not able to connect to proxy");
                Console.WriteLine(e.Message);
            }
        }
    }
}
