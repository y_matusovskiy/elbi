﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace IO
{
    public class FileWriter
    {
        private readonly string fileName;
        private readonly string path;

        private bool appendToFile;

        public FileWriter(string fileName)
        {
            this.fileName = fileName;
            this.path = AppDomain.CurrentDomain.BaseDirectory;

            appendToFile = false;
        }

        public void Write(List<int> numbers)
        {
            using (var streamWriter = new StreamWriter(path + fileName, appendToFile))
            {
                foreach (var number in numbers)
                {
                    streamWriter.WriteLine(number);
                }

                streamWriter.Close();
            }

            appendToFile = true;
        }

    }
}
