﻿using Communication;
using Communication.Enums;
using NDesk.Options;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace ProxyServer
{
    class Program
    {
        // to mock command line arguments up in debug mode:
        // go to Project Properties -> Debug -> Command Line Arguments
        // Example:
        // -t=3

        private const int _port = 17001;
        private static string thisHostName = "localhost:" + _port;

        private static MultithreadedHttpServer httpServer;

        private static int numOfThreads;

        static void Main(string[] args)
        {
            Console.WriteLine("ProxyServer\n");
            Console.WriteLine($"Command line arguments: {string.Join(" ", args)}\n");

            try
            {
                InitializeArguments(args);
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                Console.WriteLine("Terminating...");
                Console.ReadKey();
                return;
            }

            InitializeGlobals();

            Console.WriteLine("Feel free to use any of the given commands: start, exit");

            while (true)
            {
                var command = Console.ReadLine();
                switch (command)
                {
                    case "start":
                        httpServer.Start(_port);

                        Console.WriteLine($"Started on port {_port}");
                        break;
                    case "exit":
                        httpServer.Stop();

                        Console.WriteLine("Terminating...");
                        return;
                    default:
                        Console.WriteLine("Unknown command: " + command);
                        break;
                }
            }
        }

        private static void InitializeGlobals()
        {
            httpServer = new MultithreadedHttpServer(numOfThreads);
            httpServer.ProcessRequest += HttpServer_ProcessRequest;
        }

        private static void HttpServer_ProcessRequest(HttpListenerContext context)
        {
            var host = context.Request.Headers["Host"];

            string requestBody;

            using (var streamReader = new StreamReader(context.Request.InputStream))
            {
                requestBody = streamReader.ReadToEnd();
                streamReader.Close();
            }

            if (host == thisHostName)
            {
                var command = (ProxyCommand)int.Parse(requestBody);

                switch(command)
                {
                    case ProxyCommand.InitiatorStarted:
                        Console.WriteLine("InitiatorServer started");
                        break;
                    case ProxyCommand.InitiatorStopped:
                        Console.WriteLine("InitiatorServer stopped");
                        break;
                    case ProxyCommand.InitiatorExited:
                        Console.WriteLine("InitiatorServer exited");
                        break;
                }

                context.Response.StatusCode = 200;
                context.Response.Close();
            }
            else
            {
                HttpWebRequest request = (HttpWebRequest)WebRequest.Create("http://" + host);

                request.Method = "POST";
                request.Host = "localhost:17003";

                using (var streamWriter = new StreamWriter(request.GetRequestStream()))
                {
                    streamWriter.Write(requestBody);
                    streamWriter.Close();
                }

                HttpWebResponse response = (HttpWebResponse)request.GetResponse();

                if (response.StatusCode == HttpStatusCode.OK)
                {
                    string responseBody = "";
                    using (var streamReader = new StreamReader(response.GetResponseStream()))
                    {
                        responseBody = streamReader.ReadToEnd();
                    }

                    using (var streamWriter = new StreamWriter(context.Response.OutputStream))
                    {
                        streamWriter.Write(responseBody);
                        streamWriter.Close();
                    }

                    context.Response.StatusCode = 200;
                    context.Response.Close();
                }
            }
        }

        /// <exception cref="OptionException">Thrown if an argument can not be parsed</exception>
        /// <exception cref="ArgumentException">Thrown if any of the arguments is unassigned or number of sender/receiver threads is more than 3</exception>
        private static void InitializeArguments(string[] args)
        {
            var options = new OptionSet() {
                { "t=", "Threads count", (int t) => numOfThreads = t}
            };

            options.Parse(args);

            if (numOfThreads == 0 || numOfThreads > 3)
            {
                throw new ArgumentException("Incorrect command line arguments");
            }
        }
    }
}
